$(document).ready(function() {

	// --- Mail inscription participant --- //

	var contenuInscription = $('#sen_bundle_mainbundle_configmail_contenuInscription').attr('value');

	setTimeout(function(){
		$('.summernote').eq(0).code(contenuInscription);
	}, 500);
	
	$('#testInscription').click(function(){
		contenuInscription = $('.summernote').eq(0).code();
		$('#sen_bundle_mainbundle_configmail_contenuInscription').val(contenuInscription);

		objetInscription = $('#sen_bundle_mainbundle_configmail_objetInscription').val();

		mailInscription = $('#sen_bundle_mainbundle_configmail_mailInscription').val();

		$.ajax({
			type: "POST",
			url: URL,
			dataType: "JSON",
			data: { contenu: contenuInscription , objet: objetInscription, mail: mailInscription }
			})
			.done(function( msg ) {
				 if (msg.resultat == 'OK') {
				 	alert('Email de test pour l\'inscription d\'un membre a été envoyé avec succès !');
				 }
		});
	});

	// --- Mail suppression participant --- //

	var contenuDesinscription = $('#sen_bundle_mainbundle_configmail_contenuSuppression').attr('value');

	setTimeout(function(){
		$('.summernote').eq(1).code(contenuDesinscription);
	}, 500);
	
	$('#testDesinscription').click(function(){
		contenuDesinscription = $('.summernote').eq(1).code();
		$('#sen_bundle_mainbundle_configmail_contenuSuppression').val(contenuDesinscription);

		objetDesinscription = $('#sen_bundle_mainbundle_configmail_objetSuppression').val();

		mailDesinscription = $('#sen_bundle_mainbundle_configmail_mailSuppression').val();

		$.ajax({
			type: "POST",
			url: URL,
			dataType: "JSON",
			data: { contenu: contenuDesinscription , objet: objetDesinscription, mail: mailDesinscription }
			})
			.done(function( msg ) {
				 if (msg.resultat == 'OK') {
				 	alert('Email de test pour la désinscription d\'un membre a été envoyé avec succès !');
				 }
		});
	});

	// --- Mail creation annonce --- //

	var contenuCreationAnnonce = $('#sen_bundle_mainbundle_configmail_contenuCreationAnnonce').attr('value');

	setTimeout(function(){
		$('.summernote').eq(2).code(contenuCreationAnnonce);
	}, 500);
	
	$('#testCreation').click(function(){
		contenuCreationAnnonce = $('.summernote').eq(2).code();
		$('#sen_bundle_mainbundle_configmail_contenuCreationAnnonce').val(contenuCreationAnnonce);

		objetCreationAnnonce = $('#sen_bundle_mainbundle_configmail_objetCreationAnnonce').val();

		mailCreationAnnonce = $('#sen_bundle_mainbundle_configmail_mailCreationAnnonce').val();

		$.ajax({
			type: "POST",
			url: URL,
			dataType: "JSON",
			data: { contenu: contenuCreationAnnonce , objet: objetCreationAnnonce, mail: mailCreationAnnonce }
			})
			.done(function( msg ) {
				 if (msg.resultat == 'OK') {
				 	alert('Email de test pour la creation d\'une annonce a été envoyé avec succès !');
				 }
		});
	});

	// --- Mail modification annonce --- //

	var contenuModificationAnnonce = $('#sen_bundle_mainbundle_configmail_contenuModificationAnnonce').attr('value');

	setTimeout(function(){
		$('.summernote').eq(3).code(contenuModificationAnnonce);
	}, 500);
	
	$('#testModification').click(function(){
		contenuModificationAnnonce = $('.summernote').eq(3).code();
		$('#sen_bundle_mainbundle_configmail_contenuModificationAnnonce').val(contenuModificationAnnonce);

		objetModificationAnnonce = $('#sen_bundle_mainbundle_configmail_objetModificationAnnonce').val();

		mailModificationAnnonce = $('#sen_bundle_mainbundle_configmail_mailModificationAnnonce').val();

		$.ajax({
			type: "POST",
			url: URL,
			dataType: "JSON",
			data: { contenu: contenuModificationAnnonce , objet: objetModificationAnnonce, mail: mailModificationAnnonce }
			})
			.done(function( msg ) {
				 if (msg.resultat == 'OK') {
				 	alert('Email de test pour la modification d\'une annonce a été envoyé avec succès !');
				 }
		});
	});

	// --- Mail suppression annonce --- //

	var contenuSuppressionAnnonce = $('#sen_bundle_mainbundle_configmail_contenuSuppressionAnnonce').attr('value');

	setTimeout(function(){
		$('.summernote').eq(4).code(contenuSuppressionAnnonce);
	}, 500);
	
	$('#testModification').click(function(){
		contenuSuppressionAnnonce = $('.summernote').eq(4).code();
		$('#sen_bundle_mainbundle_configmail_contenuSuppressionAnnonce').val(contenuSuppressionAnnonce);

		objetSuppressionAnnonce = $('#sen_bundle_mainbundle_configmail_objetSuppressionAnnonce').val();

		mailSuppressionAnnonce = $('#sen_bundle_mainbundle_configmail_mailSuppressionAnnonce').val();

		$.ajax({
			type: "POST",
			url: URL,
			dataType: "JSON",
			data: { contenu: contenuSuppressionAnnonce , objet: objetSuppressionAnnonce, mail: mailSuppressionAnnonce }
			})
			.done(function( msg ) {
				 if (msg.resultat == 'OK') {
				 	alert('Email de test pour la suppression d\'une annonce a été envoyé avec succès !');
				 }
		});
	});

	$('#addMail').click(function() {

		contenuInscription = $('.summernote').eq(0).code();
		$('#sen_bundle_mainbundle_configmail_contenuInscription').val(contenuInscription);

		contenuDesinscription = $('.summernote').eq(1).code();
		$('#sen_bundle_mainbundle_configmail_contenuSuppression').val(contenuDesinscription);

		contenuCreationAnnonce = $('.summernote').eq(2).code();
		$('#sen_bundle_mainbundle_configmail_contenuCreationAnnonce').val(contenuCreationAnnonce);

		contenuModificationAnnonce = $('.summernote').eq(3).code();
		$('#sen_bundle_mainbundle_configmail_contenuModificationAnnonce').val(contenuModificationAnnonce);

		contenuSuppressionAnnonce = $('.summernote').eq(4).code();
		$('#sen_bundle_mainbundle_configmail_contenuSuppressionAnnonce').val(contenuSuppressionAnnonce);

	});

});