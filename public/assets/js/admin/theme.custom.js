$(document).ready(function () {
    if ($('#team_users').length > 0) {
        $('#team_users').multiselect({
            enableFiltering: true,
            filterBehavior: 'text'
        });
    }
    if ($('#team_thematics').length > 0) {
        $('#team_thematics').multiselect({
            enableFiltering: true,
            filterBehavior: 'text'
        });
    }
});

