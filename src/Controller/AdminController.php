<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Description of PageController
 *
 * @author Admin
 */
class AdminController extends AbstractController {

    public function indexAction() {
        return $this->render('dashboard.html.twig');
    }

}
