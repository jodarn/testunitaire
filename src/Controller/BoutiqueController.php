<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\CategorieRepository;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Response;



class BoutiqueController extends AbstractController {

    public function index(CategorieRepository $categorieRepository): Response {
        return $this->render('boutique.html.twig', [
            'categories' => $categorieRepository->findAll(),
        ]);

    }

    public function rayon($idCategorie, CategorieRepository $categorieRepository, ProductRepository $productRepository){
        $categorie = $categorieRepository->find($idCategorie);
        $produits = $productRepository->findBy(['categorie' => $categorie]);
        return $this->render('categorie.html.twig',[
                'categorie' => $categorie,
                'produits' => $produits,
            ]);
    }
}