<?php
// Controller/DefaultController.php
namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController {

   

    public function index(){
       /* $translator = $this->get('translator');
        $textTraduit = $translator->trans('Mon message à traduire');*/

        return $this->render('hometemp.html.twig',[

        ]);
    }

    public function hello($userName){
        return $this->render('hello.html.twig', [
            'userName' => $userName,
        ]);
    }

    public function contact(){
        return $this->render('contact.html.twig', []);
    }

    public function searchAction(Request $request, ProductRepository $productRepository): Response{
        $data = $request->request->get('search');
        $products = $productRepository->findbysearch($data);
     
     return $this->render('search.html.twig', array(
         'products' => $products));
     }    
}
