<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Service\PanierService;

class PanierController extends AbstractController {
    public function index(PanierService $panierService) {
        $produits = $panierService->getContenu();
        $total = $panierService->getTotal();
        return $this->render("panier/index.html.twig", [
            "produits" => $produits,
            "total" => $total,
        ]);
    }

    public function ajouter(PanierService $panierService, int $idProduit, int $quantite = 1) {
        $panierService->ajouterProduit($idProduit, $quantite);
        return $this->redirectToRoute('panier_index');
    }

    public function enlever(PanierService $panierService, int $idProduit, int $quantite = 1) {
        $panierService->enleverProduit($idProduit, $quantite);
        return $this->redirectToRoute('panier_index');
    }

    public function supprimer(PanierService $panierService, int $idProduit) {
        $panierService->supprimerProduit($idProduit);
        return $this->redirectToRoute('panier_index');
    }

    public function vider(PanierService $panierService) {
        $panierService->vider();
        return $this->redirectToRoute('panier_index');
    }

    public function validation(PanierService $panierService) {
        
        return $this->render("dev.html.twig");
    }
}
?>