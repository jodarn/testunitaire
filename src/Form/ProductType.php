<?php

namespace App\Form;

use App\Entity\Product;
use App\Entity\Categorie;
//use App\Form\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('price')
            ->add('categorie', EntityType::class, [
                // looks for choices from this entity
                'class' => Categorie::class,
            
                // uses the CAtegorie.nom property as the visible option string
                'choice_label' => 'nom',
                ])
            ->add('filePhotoProduct', FileType::class, array(
                'label' => 'Photo',
                'required' => true,
                'attr' => array(
                    'class' => 'file',
                    'type' => 'file',
                    'multiple' => 'false'
                )
            ))    
                ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
