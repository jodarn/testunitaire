<?php
 // src/Service/PanierService.php
namespace App\Service;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Product;
use App\Entity\Commande;
use App\Entity\LigneCommande;
use App\Entity\Usager;

// Service pour manipuler le panier et le stocker en session
class PanierService {
    ////////////////////////////////////////////////////////////////////////////
    const PANIER_SESSION = 'panier'; // Le nom de la variable de session du panier
    private $session; // Le service Session
    private $panier; // Tableau associatif idProduit => quantite
    private $em;
    // donc $this->panier[$i] = quantite du produit dont l'id = $i
    // constructeur du service
    public function __construct(SessionInterface $session,  EntityManagerInterface $em) {
        
        $this->em = $em;
        $this->session = $session;
        $this->session->start();
        // Récupération du panier en session s'il existe, init. à vide sinon
        $this->panier = $this->session->get(PanierService::PANIER_SESSION, $default = []);
    }
    // getContenu renvoie le contenu du panier
    // tableau d'éléments [ "produit" => un produit, "quantite" => quantite ]
    public function getContenu() {
        $produit_repository = $this->em->getRepository(Product::class);
        $res = [];
        foreach ($this->panier as $idProduit => $quantite) {
            $produit = $produit_repository->find($idProduit);
            $res[$idProduit] = ["produit" => $produit, "quantite" => $quantite];
        }
        return $res;
    }
    // getTotal renvoie le montant total du panier
    public function getTotal() {
        $produit_repo = $this->em->getRepository(Product::class);
        $res = 0;
        foreach ($this->panier as $idProduit => $quantite) {
            $produit = $produit_repo->find($idProduit);
            $res += $produit->getPrice() * $quantite;
        }
        return $res;
    }
    // getNbProduits renvoie le nombre de produits dans le panier
    public function getNbProduits() {
        $res = 0;
        foreach ($this->panier as $idProduit => $quantite) {
            $res += $quantite;
        }
        return $res;
    }
    // ajouterProduit ajoute au panier le produit $idProduit en quantite $quantite
    public function ajouterProduit(int $idProduit, int $quantite = 1) {
        if (!isset($this->panier[$idProduit])) {
            $quantite = $quantite;
        } else {
            $quantite = $this->panier[$idProduit] + $quantite;
        }
        $this->panier[$idProduit] = $quantite;
        $this->session->set(PanierService::PANIER_SESSION, $this->panier);
    }
    // enleverProduit enlève du panier le produit $idProduit en quantite $quantite
    public function enleverProduit(int $idProduit, int $quantite = 1) {
        if (!isset($this->panier[$idProduit]) || $this->panier[$idProduit] == 1) {
            $this->supprimerProduit($idProduit);
        } else {
            $quantite = $this->panier[$idProduit] - $quantite;
            $this->panier[$idProduit] = $quantite;
            $this->session->set(PanierService::PANIER_SESSION, $this->panier);
        }
    }
    // supprimerProduit supprime complètement le produit $idProduit du panier
    public function supprimerProduit(int $idProduit) {
        unset($this->panier[$idProduit]);
        $this->session->set(PanierService::PANIER_SESSION, $this->panier);
    }
    // vider vide complètement le panier
    public function vider() {
        $this->panier = [];
        $this->session->set(PanierService::PANIER_SESSION, $this->panier);
    }
    
}
?>