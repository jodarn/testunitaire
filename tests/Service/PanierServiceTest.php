<?php
namespace App\Service;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use App\Entity\Product;
use App\Repository\ProductRepository;

// Un service pour manipuler le contenu de la Boutique
//  qui est composée de catégories et de produits stockés "en dur"
class PanierServiceTest extends TestCase {
    
    public function testGetTotal() {
        /*
        $session = $this->getMockBuilder(SessionInterface::class)
                         ->getMock();
        
        $session->method('get')
             ->willReturn([1 => 10]);
        
        $product = $this->getMockBuilder(SessionInterface::class)
                         ->getMock();
        
        $em = $this->getMockBuilder(EntityManagerInterface::class)
                         ->getMock();
        
        $em->method('getRepository')
             ->willReturn([1 => 10]);
        
        $bt = new PanierService($session, $em);
        
        $this->assertGreaterThanOrEqual(0, $bt->getTotal());
        */
        
        $session = $this->createMock(SessionInterface::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $product = $this->createMock(Product::class);
        $productRep = $this->createMock(ProductRepository::class);

        $session->method('get')->willreturn([2=>5,3=>5]);
        $entityManager->method('getRepository')->willreturn($productRep);
        $productRep->method('find')->willreturn($product);
        $product->method('getPrice')->willreturn(1);


        $panierService = new PanierService($session,$entityManager);

        $res = $panierService->getTotal();
        
        
        $this->assertEquals(10, $res);
    }
    
    public function testGetContenu()
    {
        $session = $this->createMock(SessionInterface::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $product = $this->createMock(Product::class);
        $productRep = $this->createMock(ProductRepository::class);

        $session->method('get')->willreturn([2=>5,3=>5]);
        $entityManager->method('getRepository')->willreturn($productRep);
        $productRep->method('find')->willreturn($product);

        $panierService = new PanierService($session,$entityManager);
        
        $res =  $panierService->getContenu();
        
        $this->assertNotEmpty($res);
    }
    
    public function testGetNbProduits()
    {
        $session = $this->createMock(SessionInterface::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $product = $this->createMock(Product::class);
        $productRep = $this->createMock(ProductRepository::class);

        $session->method('get')->willreturn([2=>5,3=>5]);
        $entityManager->method('getRepository')->willreturn($productRep);
        $productRep->method('find')->willreturn($product);

        $panierService = new PanierService($session,$entityManager);

        $res = $panierService->getNbProduits();

        $this->assertEquals(10, $res);
    }
    
    public function testAjouterProduit()
    {
        $session = $this->createMock(SessionInterface::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);

        $session->method('get')->willreturn([]);

        $panierService = new PanierService($session,$entityManager);

        $panierService->ajouterProduit(2,5);
        $res = $panierService->getNbProduits();
        
        $this->assertEquals(5, $res);
    }

    public function testEnleverProduit()
    {
        $session = $this->createMock(SessionInterface::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);

        $session->method('get')->willreturn([2=>5]);

        $panierService = new PanierService($session,$entityManager);

        $panierService->enleverProduit(2,4);
        $res = $panierService->getNbProduits();

        $this->assertEquals(1, $res);
    }

    public function testSupprimerProduit(){
        $session = $this->createMock(SessionInterface::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);

        $session->method('get')->willreturn([2=>5,3=>5]);

        $panierService = new PanierService($session,$entityManager);

        $panierService->supprimerProduit(2);
        $res = $panierService->getNbProduits();
        $this->assertEquals(5, $res);
        
        $panierService->supprimerProduit(3);
        $res = $panierService->getNbProduits();
        $this->assertEmpty($res);
    }

    public function testViderPanier() {
        $session = $this->createMock(SessionInterface::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);

        $session->method('get')->willreturn([2=>5,3=>5]);

        $panierService = new PanierService($session,$entityManager);

        $panierService->vider();
        $res = $panierService->getNbProduits();
        $this->assertEmpty($res);
    }
}
